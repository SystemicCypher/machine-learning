import numpy as np
import scipy 
import pickle
import gzip
import "network.py"
import "emnist_loader.py"

mat = scipy.io.loadmat('emnist-letters.mat')
data = mat['dataset']

X_train = data['train'][0,0]['images'][0,0]
y_train = data['train'][0,0]['labels'][0,0]
X_test = data['test'][0,0]['images'][0,0]
y_test = data['test'][0,0]['labels'][0,0]

val_start = X_train.shape[0] - X_test.shape[0]
X_valid = X_train[val_start:X_train.shape[0],:]
y_valid = y_train[val_start:X_train.shape[0]]
X_train = X_train[0:val_start,:]
y_train = y_train[0:val_start]

train_data = (X_train, y_train)
test_data = (X_test, y_test)
validation_data = (X_valid, y_valid)

f = gzip.open('emnist.pkl.gz', 'w')
pickle.dump((train_data, validation_data, test_data) , f)
f.close()

train, val, test = load_data_wrapper()

#hyperparameters to adjust
epochs = 10
batch = 100
learning_rate = 1.0
hidden_nodes = 1568

net = Network([784, hidden_nodes, 26])
net.SGD(train, epochs, batch, learning_rate, test)

'''
import "network2.py"

early_stopping = 10
lmbd = 2
cost = QuadraticCost()

net = Network([784, hidden_nodes, 26], cost)
net.SGD(train, epochs, batch, learning_rate, lmbd, val, True, True, True, True, early_stopping)
'''